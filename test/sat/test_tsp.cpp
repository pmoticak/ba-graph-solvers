#include "implementation.h"

#include <graphs.hpp>
#include <sat/exec_colouring.hpp>
#include <sat/tsp.hpp>
#include <impl/basic/factory.h>

#include <cassert>

using namespace ba_graph;

int main()
{
    TspSolver lkhSolver(TspSolver::LKH_SOLVER, true, TspSolver::TSP, "LKH");
    std::vector<Graph> almostHypoham = read_graph6_file("../../resources/graphs/tsp_test_graphs/cubic_g04_32.g6").graphs();
    for (auto& G : almostHypoham) {
        assert(!is_hamiltonian_tsp(lkhSolver, G));
        int result = circumference_tsp(lkhSolver, G);
        assert(result == G.order()-1);
    }
    std::vector<Graph> graph = read_graph6_file("../../resources/graphs/tsp_test_graphs/graph_1096.g6").graphs();
    assert(circumference_tsp(lkhSolver, graph[0]) == 48);
    graph = read_graph6_file("../../resources/graphs/tsp_test_graphs/graph_28500.g6").graphs();
    assert(circumference_tsp(lkhSolver, graph[0]) == 52);
    graph = read_graph6_file("../../resources/graphs/tsp_test_graphs/graph_1059.g6").graphs();
    assert(circumference_tsp(lkhSolver, graph[0]) == 52);

    Graph H(complete_graph(5));
    assert(is_hamiltonian_tsp(lkhSolver, H));
    assert(circumference_tsp(lkhSolver, H) == 5);

    Configuration cfg;
    cfg.load_from_string(R"delim({
        "storage": {
            "dir": "../../resources/graphs"
        },
        "tsp_solvers": [
            {
                "output_type": "LKH_SOLVER",
                "execute_command": "/home/pong/src/solvers/LKH-2.0.9/LKH"
            }
        ]
    })delim");
    TspSolver lkhSolverHcp(cfg,0);

    H = std::move(circuit(10));
    assert(is_hamiltonian_hcp(lkhSolverHcp, H));

    H = std::move(circuit(5));
    assert(is_hamiltonian_hcp(lkhSolverHcp, H));

    H = std::move(create_petersen());
    assert(!is_hamiltonian_hcp(lkhSolverHcp, H));
}
