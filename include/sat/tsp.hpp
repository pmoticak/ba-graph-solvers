#ifndef BA_GRAPH_SAT_TSP_HPP
#define BA_GRAPH_SAT_TSP_HPP

#include "../config/configuration.hpp"
#include "../invariants/girth.hpp"
#include "../util/system.hpp"

#include <filesystem>
#include <fstream>
#include <regex>
#include <vector>
#include <cassert>

namespace ba_graph {

static const int MAX_WEIGHT = 10000000;

class TspSolver {
public:
    enum OutputType {
        LKH_SOLVER,
        CONCORDE
    };
    enum Mode {
        TSP,
        HCP
    };

    OutputType outputType;
    bool external;
    Mode mode;
    std::string command;

    TspSolver(OutputType ot, bool external, Mode mode, std::string cmd = "")
        : outputType(ot)
        , external(external)
        , mode(mode)
        , command(cmd)
    {
#ifdef BA_GRAPH_DEBUG
        if (external)
            assert(command != "");
        assert(outputType == LKH_SOLVER);
#endif
    }

    TspSolver(Configuration cfg, int index)
    {
        auto ot = cfg.tsp_solver_output_type(index);
        if (ot == "LKH_SOLVER") {
            outputType = LKH_SOLVER;
        } else if (ot == "CONCORDE") {
            throw std::runtime_error("unsupported tsp solver output type/WIP");
        } else {
            throw std::runtime_error("unsupported tsp solver output type");
        }
        command = cfg.tsp_solver_command(index);
#ifdef BA_GRAPH_DEBUG
        if (external)
            assert(command != "");
        assert(outputType == LKH_SOLVER);
#endif
    }

    void set_mode(Mode m) { this->mode = m; }
};

std::vector<std::vector<int>> graph_to_upper_row_matrix(Graph& G)
{
#ifdef BA_GRAPH_DEBUG
    assert(!has_loop(G));
    assert(!has_parallel_edge(G));
#endif
    std::vector<Edge> edges = G.list(RP::all(), IP::primary(), IT::e());
    std::vector<std::vector<int>> matrix(G.order(), std::vector<int>(G.order(),MAX_WEIGHT));
    int offset = edges[0].v1().to_int();
    int u,v;
    for (Edge e : edges) {
    	u = e.v1().to_int();
    	v = e.v2().to_int();
    	if (u >= v) {
    	    matrix[v-offset][u-offset] = 1;
    	} else {
    	    matrix[u-offset][v-offset] = 1;
    	}    
    }
    return matrix;
}

std::string write_edges_hcp_stream(Graph& G)
{
#ifdef BA_GRAPH_DEBUG
    assert(!has_loop(G));
    assert(!has_parallel_edge(G));
#endif
    renumber_consecutive(G);
    std::stringstream out;
    std::vector<Edge> edges = G.list(RP::all(), IP::primary(), IT::e());
    int offset = edges[0].v1().to_int() - 1;
    for (Edge e : edges) {
        out << e.v1().to_int() - offset << " " << e.v2().to_int() - offset << std::endl;
    }
    out << -1 << std::endl;
    return out.str();
}

std::string write_matrix_tsp_stream(const std::vector<std::vector<int>>& matrix,
    const std::vector<bool>& mask)
{
    std::stringstream out;
    for (unsigned int i = 0; i < matrix.size() - 1; ++i) {
        if (mask[i]) continue;
        for (unsigned int j = i + 1; j < matrix.size(); ++j) {
            if (mask[j]) continue;
            out << matrix[i][j] << " ";
        }
        out << std::endl;
    }
    return out.str();
} 

void write_tsplib_file(TspSolver& solver, Graph& G, const std::string fileName,
    const std::vector<std::vector<int>>& matrix, const std::vector<bool>& mask,
    const std::string comment = "")
{
    std::ofstream file;
    if (!file) {
        throw std::runtime_error("cannot write file " + fileName);
    }
    file.open(fileName, std::ofstream::out);
    file << "NAME : " << fileName << std::endl;
    file << "COMMENT : " << comment << std::endl;
    if (solver.mode == solver.HCP) {
        file << "DIMENSION : " << G.order() << std::endl;
        file << "TYPE : HCP" << std::endl;
        file << "EDGE_DATA_FORMAT : EDGE_LIST" << std::endl;
        file << "EDGE_DATA_SECTION" << std::endl;
        file << write_edges_hcp_stream(G) << std::endl;
    } else if (solver.mode == solver.TSP) {
        if (matrix.empty()) {
            file.close();
            std::remove(fileName.c_str());
            throw std::logic_error("graph upper row matrix not provided");
        }
        file << "DIMENSION : " << matrix[0][0] << std::endl;
        file << "TYPE : TSP" << std::endl;
        file << "EDGE_WEIGHT_TYPE : EXPLICIT" << std::endl;
        file << "EDGE_WEIGHT_FORMAT : UPPER_ROW" << std::endl;
        file << "EDGE_WEIGHT_SECTION" << std::endl;
        file << write_matrix_tsp_stream(matrix, mask) << std::endl;
    }
    file << "EOF" << std::endl;
    file.close();
}

std::string write_parameter_file(const std::string& tsplibFile, const std::string& fileName)
{
    std::ofstream file;
    if (!file) {
        throw std::runtime_error("cannot write file " + fileName);
    }
    file.open(fileName, std::ofstream::out);
    file << "PROBLEM_FILE = " << tsplibFile << std::endl;
    std::string outFile = fileName;
    outFile.append(".sol.txt");
    file << "TOUR_FILE = " << outFile << std::endl;
    return outFile;
}

bool solvable(TspSolver& solver, Graph& G, 
    const std::vector<std::vector<int>>& matrix = std::vector<std::vector<int>>(),
    const std::vector<bool>& mask = std::vector<bool>(), 
    const std::string& tmp_dir = ".")
{
    if (solver.mode == solver.TSP && matrix.empty()) {
        throw std::logic_error("graph upper row matrix not provided");
    }
    if (G.order() < 3) return false;
    if (solver.external) {
        std::string tsplibFile, parameterFile, outFile;

        tsplibFile = tmp_file(tmp_dir).string();
        parameterFile = tmp_file(tmp_dir).string();
        write_tsplib_file(solver, G, tsplibFile, matrix, mask);
        outFile = write_parameter_file(tsplibFile, parameterFile);

        std::string solveCommand = solver.command + " " + parameterFile;
        std::string result = internal::execute_command(solveCommand.c_str());
        std::remove(tsplibFile.c_str());
        std::remove(parameterFile.c_str());
        std::remove(outFile.c_str());
        if (solver.outputType == solver.LKH_SOLVER) {
            if (solver.mode == solver.HCP) {
                std::regex successes("Successes\\/Runs = [1-9]\\d*\\/\\d+");
                if (std::regex_search(result, successes)) return true;
            } else if (solver.mode == solver.TSP) {
                std::smatch min_cost_matches;
                std::regex min_cost("Cost\\.min = ([0-9]+),");
                if (!std::regex_search(result, min_cost_matches, min_cost)) return false;
                if (matrix[0][0] == std::stoi(min_cost_matches[1])) return true;
            }
            return false;
        } else {
            throw std::runtime_error("unsupported external tsp solver output type");
        }    
    }
    throw std::runtime_error("unsupported tsp solver type");
}

bool is_hamiltonian_hcp(TspSolver& solver, Graph& G)
{
    if (G.order() == 0) return true;
    if (G.order() == 1) return has_loop(G);
    if (G.order() == 2) return has_parallel_edge(G);
    solver.set_mode(solver.HCP);
    return solvable(solver, G);
}

inline bool is_hamiltonian_tsp(TspSolver& solver, Graph& G)
{
    if (G.order() == 0) return true;
    if (G.order() == 1) return has_loop(G);
    if (G.order() == 2) return has_parallel_edge(G);
    solver.set_mode(solver.TSP);
    std::vector<std::vector<int>> matrix;
    matrix = graph_to_upper_row_matrix(G);
    matrix[0][0] = G.order();
    std::vector<bool> mask(matrix.size(), false);
    return solvable(solver, G, matrix, mask);
}

inline int circumference_tsp(TspSolver& solver, Graph& G)
{
    solver.set_mode(solver.TSP);
    if (is_hamiltonian_tsp(solver, G)) return G.order();
    std::vector<std::vector<int>> matrix;
    matrix = graph_to_upper_row_matrix(G);
    std::vector<bool> mask(matrix.size(), false);
    matrix[0][0] = G.order();

    for (int i = mask.size()-1; i >= 0; --i) {
        mask[i] = true;
        --matrix[0][0];
        if (matrix[0][0] < 3) return 0;
        do {
            if (solvable(solver, G, matrix, mask)) {
                return G.order()-(mask.size()-i);
            }  
        } while (std::next_permutation(mask.begin(), mask.end()));
    }
    return -1;
}

} // namespace ba_graph

#endif
